import org.junit.Test;

import studiplayer.audio.TaggedFile;
import studiplayer.basic.TagReader;

import static org.junit.Assert.assertEquals;

import java.util.Map;

public class UTestTaggedFile {
    @Test
    public void test_play_01() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/beethoven-ohne-album.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_02() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Eisbach Deep Snow.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_03() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Haydn - Symphonie # 96 Motiv.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_04() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/kein.ogg.sondern.wav");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_05() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/kein.wav.sondern.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_06() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/MadreDFD.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_07() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Meltdown.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_08() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Motiv 5. Symphonie von Beethoven.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_09() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Mozart - Piano Concerto No25 Motiv.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_10() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Road Movie.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_11() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Rock 812.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_12() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/tanom p2 journey.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_13() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/wellenmeister_awakening.ogg");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_14() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/wellenmeister - fire.mp3");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_play_15() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/wellenmeister - tranquility.wav");
        tf.play();
        // Note: cancel playback in eclipse console
    }

    @Test
    public void test_timeFormatter_01() throws Exception {
        assertEquals("Wrong time format", "05:05", TaggedFile.timeFormatter(305862000L));
    }

    @Test // @Ignore
    public void test_readTags_01() throws Exception {
        TaggedFile tf = new TaggedFile("audiofiles/Rock 812.mp3");
        Map<String, Object> tag_map = TagReader.readTags(tf.getPathname());
        for (String key : tag_map.keySet()) {
            System.out.printf("\nKey: %s\n", key);
            System.out.printf("Type of value %s\n", tag_map.get(key).getClass().toString());
            System.out.println("Value: " + tag_map.get(key));
        }
    }

    @Test
    public void test_readAndStoreTags_01() throws Exception {
        TaggedFile tf = new TaggedFile();
        tf.readAndStoreTags("audiofiles/Rock 812.mp3");
        assertEquals("Wrong author", "Eisbach", tf.getAuthor());
        assertEquals("Wrong title", "Rock 812", tf.getTitle());
        assertEquals("Wrong album", "The Sea, the Sky", tf.getAlbum());
        assertEquals("Wrong time format", "05:31", tf.getFormattedDuration());
    }
}
