import org.junit.Test;

import studiplayer.audio.WavFile;

import static org.junit.Assert.assertEquals;

public class UTestWavFile {
    @Test
    public void test_computeDuration_01() throws Exception {
        assertEquals("Wrong Duration", 2000000L, WavFile.computeDuration(88200L, 44100.0f));
    }

    @Test
    public void test_readAndSetDurationFromFile_01() throws Exception {
        WavFile wf = new WavFile();
        wf.parsePathname("audiofiles/wellenmeister - tranquility.wav");
        wf.readAndSetDurationFormFile(wf.getPathname());
        assertEquals("Wrong time format", "02:21", wf.getFormattedDuration());
    }
}
