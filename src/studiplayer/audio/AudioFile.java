package studiplayer.audio;

import java.io.File;

public abstract class AudioFile {
    private String filename = "";
    private String pathname = "";
    private char sepchar = java.io.File.separatorChar;

    private boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
    }

    public AudioFile() {
    }

    public AudioFile(String myString) throws NotPlayableException {
        parsePathname(myString);
        parseFilename(getFilename());
        File file = new File(getPathname());
        if (file.canRead() == false) {
            throw new NotPlayableException(this.pathname, "can't read Path:<" + getPathname() + ">");
        }
    }

    public void parsePathname(String myString) {
        int left_border = 0;

        if (myString.length() >= 2) {
            if (isWindows() && Character.isLetter(myString.charAt(0)) && myString.charAt(1) == ':') {
                pathname = myString.charAt(0) + ":";
                myString = myString.substring(2);
            } else if (Character.isLetter(myString.charAt(0)) && myString.charAt(1) == ':') {
                pathname = "" + sepchar + myString.charAt(0);
                myString = myString.substring(2);
            }
        }
        filename = myString;
        for (int i = left_border; i < myString.length(); i++) {
            if (myString.charAt(i) == '/' || myString.charAt(i) == '\\') {
                pathname = pathname + myString.substring(left_border, i) + sepchar;

                while (i < myString.length() && (myString.charAt(i) == '/' || myString.charAt(i) == '\\')) {
                    i++;
                }
                left_border = i;
                filename = myString.substring(left_border, myString.length());
            }
        }

        pathname = pathname + filename;
        return;
    }

    public String getPathname() {
        return pathname;
    }

    public String getFilename() {
        return filename;
    }

    protected String author = "";
    protected String title = "";

    private String remove_blanks(String myString) {
        // leading blanks:
        while (true) {
            if (myString.length() > 0 && myString.charAt(0) == ' ') {
                myString = myString.substring(1, myString.length());
            } else
                break;

        }
        // last blanks:
        while (true) {
            if (myString.length() > 0 && myString.charAt(myString.length() - 1) == ' ') {
                myString = myString.substring(0, myString.length() - 1);
            } else
                break;
        }
        return myString;
    }

    private String remove_extension(String myString) {
        int i;
        for (i = myString.length() - 1; i >= 0; i--) {
            if (myString.charAt(i) == '.') {
                myString = myString.substring(0, i);
                break;
            }
        }
        return myString;
    }

    public void parseFilename(String myString) {
        int separator_pos = -2;
        for (int i = 1; i < myString.length() - 1; i++) {
            if (" - ".equals(myString.substring(i - 1, i + 2))) {
                separator_pos = i;
                break;
            }
        }
        if (separator_pos <= 1) {
            /* no operation */} else {
            author = myString.substring(0, separator_pos);
        }
        if (separator_pos < 1) {
            title = myString;
        } else {
            title = myString.substring(separator_pos + 1, myString.length());
        }
        // author and title now separated, clean them up:
        title = remove_extension(title);
        author = remove_blanks(author);
        title = remove_blanks(title);
        return;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        if (getAuthor().length() == 0)
            return getTitle();
        else
            return getAuthor() + " - " + getTitle();
    }

    public abstract void play() throws NotPlayableException;

    public abstract void togglePause();

    public abstract void stop();

    public abstract String getFormattedDuration();

    public abstract String getFormattedPosition();

    public abstract String[] fields();
}
