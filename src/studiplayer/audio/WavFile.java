package studiplayer.audio;

import studiplayer.basic.WavParamReader;

public class WavFile extends SampledFile {

    public WavFile() {
        super();
    }

    public WavFile(String pathname) throws NotPlayableException {
        super(pathname);
        readAndSetDurationFormFile(super.getPathname());
    }

    public static long computeDuration(long NumberOfFrames, float frameRate) {
        return (long) ((1000000 * NumberOfFrames) / frameRate); // don't change
                                                                // brackets,
                                                                // this is
                                                                // fragile!
    }

    public void readAndSetDurationFormFile(String pathname) throws NotPlayableException {
        float frameRate = -1;
        long numberOfFrames = -1;
        try {
            WavParamReader.readParams(pathname);
        } catch (Exception e) {
            throw new NotPlayableException(pathname, "WavParamReader.readParams(pathname) failed", e);
        }
        frameRate = WavParamReader.getFrameRate();
        numberOfFrames = WavParamReader.getNumberOfFrames();
        super.duration = "" + computeDuration(numberOfFrames, frameRate);
    }

    public String toString() {
        return super.toString() + " - " + super.getFormattedDuration();
    }

    public String[] fields() {
        String[] tmp = new String[4];
        tmp[0] = super.getAuthor();
        tmp[1] = super.getTitle();
        tmp[2] = ""; // wav file has no album
        tmp[3] = super.getFormattedDuration();
        return tmp;
    }
}
