package studiplayer.audio;

import java.util.Map;

public class TaggedFile extends SampledFile {

    private String album = "";

    public TaggedFile() {
        super();
    }

    public TaggedFile(String pathname) throws NotPlayableException {
        super(pathname);
        this.readAndStoreTags(super.getPathname());
    }

    public String getAlbum() {
        return this.album;
    }

    public void readAndStoreTags(String pathname) throws NotPlayableException {
        Map<String, Object> tag_map = null;
        try {
            tag_map = studiplayer.basic.TagReader.readTags(pathname);
        } catch (Exception e) {
            throw new NotPlayableException(pathname, "studiplayer.basic.TagReader.readTags(pathname) failed", e);
        }
        if (tag_map.get("title") != null && tag_map.get("title").toString().length() > 0
                && tag_map.get("title").toString().equals("0") == false) {
            super.title = tag_map.get("title").toString().trim();
        }
        if (tag_map.get("author") != null && tag_map.get("author").toString().length() > 0
                && tag_map.get("author").toString().equals("0") == false) {
            super.author = tag_map.get("author").toString().trim();
        }
        if (tag_map.get("album") != null && tag_map.get("album").toString().length() > 0
                && tag_map.get("album").toString().equals("0") == false) {
            this.album = tag_map.get("album").toString().trim();
        }
        if (tag_map.get("duration") != null && tag_map.get("duration").toString().length() > 0
                && tag_map.get("duration").toString().equals("0") == false) {
            super.duration = tag_map.get("duration").toString();
        }
    }

    public String toString() {
        if (album.length() == 0) { // no album found
            return super.toString() + " - " + super.getFormattedDuration();
        } else {
            return super.toString() + " - " + this.album + " - " + super.getFormattedDuration();
        }
    }

    public String[] fields() {
        String[] tmp = new String[4];
        tmp[0] = super.getAuthor();
        tmp[1] = super.getTitle();
        tmp[2] = this.album;
        tmp[3] = super.getFormattedDuration();
        return tmp;
    }
}
