package studiplayer.ui;

import studiplayer.audio.AudioFile;
import studiplayer.audio.NotPlayableException;
import studiplayer.audio.PlayList;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Player extends JFrame implements ActionListener {
    private final char sepchar = java.io.File.separatorChar;

    private final String NO_HEADLINE = "Studiplayer: empty play list";
    private final String NO_SONG = "no current song";
    private final String CUR_SONG_PREFIX = "Current song: ";
    private final String NO_PLAYTIME = "--:--";
    private final String ZERO_PLAYTIME = "00:00";
    // not using sepchar ('/' or '\' ????) !!!!!!!!
    // can't use in main otherwise:
    public static final String DEFAULT_PLAYLIST = "playlists/DefaultPlayList.m3u";

    private PlayList playList = null;
    private PlayListEditor playListEditor;
    // north and west Labels
    private JLabel songDescription;
    private JLabel playTime;
    private volatile boolean stopped;
    private boolean editorVisible;
    JButton play;
    JButton pause;
    JButton stop;

    public Player(PlayList playList) {
        this.playList = playList;
        editorVisible = false;
        playListEditor = new PlayListEditor(this, this.playList);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopped = true;
        AudioFile af = playList.getCurrentAudioFile();
        songDescription = new JLabel();
        playTime = new JLabel();
        super.add(songDescription, BorderLayout.NORTH);
        super.add(playTime, BorderLayout.WEST);
        updateSongInfo(af);
        // group buttons on JPanel buttons:
        JPanel buttons = new JPanel();
        play = new JButton(new ImageIcon("icons" + sepchar + "play.png"));
        play.setActionCommand("AC_PLAY");
        play.addActionListener(this);
        play.setEnabled(true);
        // play:
        buttons.add(play);
        pause = new JButton(new ImageIcon("icons" + sepchar + "pause.png"));
        pause.setActionCommand("AC_PAUSE");
        pause.addActionListener(this);
        pause.setEnabled(false);
        // pause:
        buttons.add(pause);
        stop = new JButton(new ImageIcon("icons" + sepchar + "stop.png"));
        stop.setActionCommand("AC_STOP");
        stop.addActionListener(this);
        stop.setEnabled(false);
        buttons.add(stop);
        // stop:
        JButton next = new JButton(new ImageIcon("icons" + sepchar + "next.png"));
        next.setActionCommand("AC_NEXT");
        next.addActionListener(this);
        next.setEnabled(true);
        buttons.add(next);
        // pl_editor:
        JButton pl_editor = new JButton(new ImageIcon("icons" + sepchar + "pl_editor.png"));
        pl_editor.setActionCommand("AC_PL_EDITOR");
        pl_editor.addActionListener(this);
        pl_editor.setEnabled(true);
        buttons.add(pl_editor);
        // place buttons in center
        super.add(buttons, BorderLayout.CENTER);
        
        // Pack neat and make visible
        this.pack();
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent input) {
        AudioFile af;
        String cmd = input.getActionCommand();

        if (cmd.equals("AC_PLAY")) {
            play.setEnabled(false);
            pause.setEnabled(true);
            stop.setEnabled(true);
            this.playCurrentSong();
        } else if (cmd.equals("AC_PAUSE")) {
            play.setEnabled(false);
            pause.setEnabled(true);
            stop.setEnabled(true);
            af = playList.getCurrentAudioFile();
            if(playList.isEmpty() == false){
                playList.getCurrentAudioFile().togglePause();
            }
            System.out.println("Pausing " + af.toString());
            System.out.println("Filename is " + af.getFilename());
            System.out.println("Current index is " + playList.getCurrent());
        } else if (cmd.equals("AC_STOP")) {
            play.setEnabled(true);
            pause.setEnabled(false);
            stop.setEnabled(false);
            this.stopCurrentSong();
        } else if (cmd.equals("AC_NEXT")) {
            play.setEnabled(false);
            pause.setEnabled(true);
            stop.setEnabled(true);
            if (stopped == false) {
                this.stopCurrentSong();
            }

            playList.changeCurrent();
            this.playCurrentSong();
            af = playList.getCurrentAudioFile();
            if (af != null) {
                System.out.println("Switched to next audio file ");
            } else {
                System.out.println("Playlist is empy, couldn't switch");
            }
            System.out.println("");
            // System.out.println("Switching to next audio file " +
            // af.toString());
            // System.out.println("Filename is " + af.getFilename());
            // System.out.println("Current index is " + playList.getCurrent());
        } else if (cmd.equals("AC_PL_EDITOR")) {
            if(editorVisible){
                editorVisible = false;
            }else {
                editorVisible = true;
            }
            playListEditor.setVisible(editorVisible);
            System.out.println("PL_EDITOR");
        } else {
            System.out.println("!!!Invalid input, no action performed!!!");
        }
    }

    private void updateSongInfo(AudioFile af) {
        if (af == null) {
            setTitle(this.NO_HEADLINE);
            songDescription.setText(this.NO_SONG);
            playTime.setText(this.NO_PLAYTIME);
        } else {
            String song_info = af.toString();

            setTitle(this.CUR_SONG_PREFIX + song_info);
            songDescription.setText(song_info);
            playTime.setText(this.ZERO_PLAYTIME);

        }
        // this.setVisible(true);
    }

    private void playCurrentSong() {
        AudioFile af;
        af = playList.getCurrentAudioFile();
        updateSongInfo(af);
        if (af != null) {
            this.stopped = false;
            // Start Threads
            (new TimerThread()).start();
            (new PlayerThread()).start();
        }
        System.out.println("Playing " + af.toString());
        System.out.println("Filename is " + af.getFilename());
        System.out.println("Current index is " + playList.getCurrent());
    }

    private void stopCurrentSong() {
        
        AudioFile af;
        af = playList.getCurrentAudioFile();
        af.stop();
        updateSongInfo(af);
        this.stopped = true;
        System.out.println("Stopping " + af.toString());
        System.out.println("Filename is " + af.getFilename());
        System.out.println("Current index is " + playList.getCurrent());
    }

    private class TimerThread extends Thread {
        public void run() {
            while (stopped == false) {
                AudioFile af = playList.getCurrentAudioFile();
                playTime.setText(af.getFormattedPosition());
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    // NOOP ?
                }
            }
        }
    }

    private class PlayerThread extends Thread {
        public void run() {
            while (stopped == false && playList.isEmpty() == false) {
                try {
                    AudioFile af = playList.getCurrentAudioFile();
                    af.play();
                    if(stopped == false){
                        //playList.changeCurrent(); mysterious seite 14
                        updateSongInfo(playList.getCurrentAudioFile());
                    }
                    
                } catch (NotPlayableException e) {
                    e.printStackTrace();
                }
                
            }
        }
    }

    public static void main(String[] args) {
        PlayList playList = new PlayList();
        if (args.length > 1 && args[1] != null) {
            playList.loadFromM3U(args[1]);
        } else {
            playList.loadFromM3U(DEFAULT_PLAYLIST);
        }
        new Player(playList);
    }
}
